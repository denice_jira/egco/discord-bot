import { Client, DMChannel, Guild, GuildMember, Message, NewsChannel, TextChannel } from 'discord.js'

export class TextMessage extends Message {
	constructor(client: Client, data: Record<string, unknown>, channel: TextChannel) {
		super(client, data, channel)
	}

	public readonly guild: Guild
	public readonly member: GuildMember
	public channel: TextChannel
}

export class DMMessage extends Message {
	constructor(client: Client, data: Record<string, unknown>, channel: DMChannel) {
		super(client, data, channel)
	}

	public readonly guild: null
	public readonly member: null
	public channel: DMChannel
}

export class NewsMessage extends Message {
	constructor(client: Client, data: Record<string, unknown>, channel: NewsChannel) {
		super(client, data, channel)
	}

	public channel: NewsChannel
}
