type RequiredEnvField = 'DISCORD_TOKEN' | 'DB_HOST' | 'DB_PORT' | 'DB_NAME' | 'DB_USERNAME' | 'DB_PASS' | 'SERVER_NAME' | 'GUILD_ID'
type OptionalEnvField = 'DB_LOGGING_ENABLE'

declare namespace NodeJS {
	type EnvFile = {
		[RK in RequiredEnvField]: string
	} &
		{
			[OK in OptionalEnvField]?: string
		}

	// eslint-disable-next-line @typescript-eslint/no-empty-interface
	interface ProcessEnv extends EnvFile {}
}
