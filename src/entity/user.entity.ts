import { BaseEntity, Column, Entity } from 'typeorm'

@Entity('user')
export class User extends BaseEntity {
	@Column('bigint', { name: 'user_id', primary: true })
	userId: number

	@Column('bigint', { name: 'student_id' })
	studentId: number

	@Column('varchar', { name: 'nick_name', nullable: true })
	nickName: string

	@Column('bigint', { name: 'role' })
	role: number

	@Column('timestamp', {
		name: 'updated_at',
		default: () => 'CURRENT_TIMESTAMP',
	})
	updatedAt: Date

	@Column('varchar', { name: 'updated_by', length: 100 })
	updatedBy: string

	@Column('timestamp', {
		name: 'created_at',
		default: () => 'CURRENT_TIMESTAMP',
	})
	createdAt: Date

	@Column('varchar', { name: 'created_by', length: 100 })
	createdBy: string
}
