import { Class } from 'utility-types'

export function classInstanceOf<C extends P, P>(child: Class<C | unknown>, parent: Class<P>): child is Class<C> {
	return child.prototype instanceof parent || child === parent
}
