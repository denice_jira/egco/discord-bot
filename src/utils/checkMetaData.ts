export function checkMetaData<T>(
	// eslint-disable-next-line @typescript-eslint/ban-types
	target: Object,
	key: unknown,
	predicate: (metaData: T) => boolean,
	propertyKey?: string | symbol,
): boolean {
	const metaData: T | undefined = Reflect.getMetadata(key, target, propertyKey)
	if (metaData === undefined) return false
	return predicate(metaData)
}

export function checkMetaDataOrUndefined<T>(
	// eslint-disable-next-line @typescript-eslint/ban-types
	target: Object,
	key: unknown,
	predicate: (metaData: T) => boolean,
	propertyKey?: string | symbol,
): boolean {
	const metaData: T | undefined = Reflect.getMetadata(key, target, propertyKey)
	if (metaData === undefined) return true
	return predicate(metaData)
}
