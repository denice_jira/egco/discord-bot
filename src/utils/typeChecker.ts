// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function isFunction<V extends (...args: A) => R, A extends any[], R>(value: unknown | V): value is V {
	return typeof value === 'function'
}
