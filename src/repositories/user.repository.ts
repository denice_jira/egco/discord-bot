import { User } from 'src/entity'
import { EntityRepository, InsertResult, Repository } from 'typeorm'

@EntityRepository(User)
export class UsersRepository extends Repository<User> {
	async insertOrOverrideStudent(userId: number, studentId: number): Promise<InsertResult> {
		const existingUser = await this.findOne({ userId })
		if (existingUser) await this.remove(existingUser)

		const newUser = this.create({ userId, studentId, role: 1, updatedBy: 'bot', createdBy: 'bot' })
		return this.insert(newUser)
	}

	async updateNickName(userId: number, nickName: string): Promise<User> {
		const existingUser = await this.findOneOrFail({ userId })

		existingUser.nickName = nickName

		return existingUser.save()
	}
}
