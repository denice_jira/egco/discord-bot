export const enum MetadataTypes {
	Type = 'design:type',
	Paramtypes = 'design:paramtypes',
	ReturnType = 'design:returntype',
}

export const enum DecoratorTypes {
	On = '__on_decorator__',
}
