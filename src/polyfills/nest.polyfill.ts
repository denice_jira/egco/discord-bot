import { Logger } from '@nestjs/common'

Object.defineProperties(Logger.prototype, {
	setContext: {
		enumerable: true,
		value(this: Logger, context: string) {
			this.context = context
		},
	},
})
