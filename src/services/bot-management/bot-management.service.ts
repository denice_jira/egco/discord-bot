import { Injectable } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { Client, ClientProvider, On } from 'discord-nestjs'
import { DiscordJSService } from 'src/decorators/filters/discord-js-service'
import { DMMessage, TextMessage } from 'src/types/Message.type'

@DiscordJSService()
@Injectable()
export class BotManagement {
	@Client()
	private discordProvider: ClientProvider

	constructor(private configService: ConfigService) {}
	@On({ event: 'message' })
	public async onMessage(message: DMMessage): Promise<void> {
		if (message.author.bot) return
		if (message.content === '/info guild') {
			const client = this.discordProvider.getClient().guilds.cache.find(guild => guild.id === this.configService.get('GUILD_ID'))
			message.author.send(JSON.stringify(client))
		}
	}
	@On({ event: 'message' })
	public async onMessageText(message: TextMessage): Promise<void> {
		if (message.author.bot) return
		console.log(message.content)
	}
}
