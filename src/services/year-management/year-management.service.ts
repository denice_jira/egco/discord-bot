import { Injectable } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { DiscordClientProvider, On } from 'discord-nestjs'
import { Guild, GuildMember, User } from 'discord.js'
import { UsersRepository } from 'src/repositories/user.repository'
import { UserState, YearManagementUserStatus } from 'src/state/userState'
import { NoBot } from '../../decorators/filters'
import { DiscordJSService } from '../../decorators/filters/discord-js-service'
import { DMMessage } from '../../types/Message.type'

@DiscordJSService()
@Injectable()
export class YearManagementService {
	private readonly serverName: string
	private guild: Guild

	private readonly YEAR_OFFSET: number = 32

	constructor(
		private configService: ConfigService<NodeJS.ProcessEnv>,
		private userState: UserState,
		private usersRepository: UsersRepository,
		private discordProvider: DiscordClientProvider,
	) {
		this.serverName = this.configService.get('SERVER_NAME')
	}

	async greetUser(user: User): Promise<void> {
		await this.userState.clearUserState(user.id)
		await this.userState.pushUserState(user.id, YearManagementUserStatus.ROLE_ASSIGNMENT)

		await user.send(`Welcome to ${this.serverName}. There are some questions needed to be asked to set your student status properly.`)
		return user.send('What is your student id? Ex. 59xxxxx').then()
	}

	@NoBot()
	@On({ event: 'message' })
	public async onDMMessage(message: DMMessage): Promise<void> {
		if (message.content === '/register') return this.greetUser(message.author)
		this.guild = await this.discordProvider.getClient().guilds.cache.get(this.configService.get('GUILD_ID'))
		switch (await this.userState.getUserStateCurrent(message.author.id)) {
			case YearManagementUserStatus.ROLE_ASSIGNMENT: {
				const matchedStudentId = message.content.match(/^([4-9][0-9])[0-9]{5}$/)

				if (!matchedStudentId) return message.author.send('Please input a valid student id (59xxxxx)').then()

				const MUYear = parseInt(matchedStudentId[1])
				if (MUYear > 70 || MUYear < 40)
					return message.author.send('Your student year is out of scope. If it is your real student id please contact admin.').then()

				await this.userState.popUserState(message.author.id)

				const EGYear = MUYear - this.YEAR_OFFSET
				const role = this.guild.roles.cache.find(role => role.name === `CO${EGYear}`)

				if (!role)
					return message.author
						.send('Role for your student year is not exist yet. Please contact admin to create a appropriate year for you.')
						.then()

				const user = await this.guild.members.fetch(message.author)
				try {
					await user.roles.add(role)
				} catch (error) {
					return message.author.send('Error 403. Please contact admin').then()
				}

				await message.author.send(`You are now assigned to CO${EGYear}.`)
				await this.usersRepository.insertOrOverrideStudent(parseInt(message.author.id), parseInt(matchedStudentId[0]))

				await this.userState.pushUserState(message.author.id, YearManagementUserStatus.NAME_ASSIGNMENT)
				return message.author.send('What is your nickname?').then()
			}

			case YearManagementUserStatus.NAME_ASSIGNMENT: {
				if (message.content.length > 50)
					return message.author.send('Your name is too long. Please input name shorter than 50 characters').then()

				await this.userState.popUserState(message.author.id)
				const userStorage = await this.userState.getStorage(message.author.id)
				userStorage.nickname = message.content
				await this.userState.setStorage(message.author.id, userStorage)

				await this.userState.pushUserState(message.author.id, YearManagementUserStatus.NAME_CONFIRMATION)
				return message.author.send(`Your nickname will be "${message.content}". Confirm? (Yes/No)`).then()
			}

			case YearManagementUserStatus.NAME_CONFIRMATION: {
				switch (message.content.toUpperCase()) {
					case 'YES': {
						const { nickname } = await this.userState.getStorage(message.author.id)
						await this.usersRepository.updateNickName(parseInt(message.author.id), nickname)
						return message.author.send(`Your nickname is to ${nickname}`).then()
					}

					case 'NO': {
						await this.userState.popUserState(message.author.id)
						await this.userState.pushUserState(message.author.id, YearManagementUserStatus.NAME_ASSIGNMENT)
						return message.author.send('What is your nickname?').then()
					}
				}
			}
		}
	}

	@On({ event: 'guildMemberAdd' })
	public async onNewMember(member: GuildMember): Promise<void> {
		if (member.guild.id !== this.configService.get('GUILD_ID')) return

		return this.greetUser(member.user)
	}
}
