import { Injectable } from '@nestjs/common'

export enum YearManagementUserStatus {
	ROLE_ASSIGNMENT = 1,
	NAME_ASSIGNMENT = 2,
	NAME_CONFIRMATION = 3,
	FINISHED = 4,
}

interface storage {
	nickname?: string
}

interface state {
	state: YearManagementUserStatus[]
	storage: storage
}

@Injectable()
export class UserState {
	private stateUser: Map<string, state>

	constructor() {
		this.stateUser = new Map()
	}

	public async getUserStateAll(username: string): Promise<state> {
		return this._getUserState(username)
	}

	public async getUserStateCurrent(username: string): Promise<YearManagementUserStatus> {
		const userState = this._getUserState(username).state
		return userState[userState.length - 1]
	}

	public async popUserState(username: string): Promise<YearManagementUserStatus> {
		const userState = this._getUserState(username)
		const pop = userState.state.pop()
		this.stateUser.set(username, userState)
		return pop
	}

	public async pushUserState(username: string, state: YearManagementUserStatus): Promise<void> {
		const userState = this._getUserState(username)
		userState.state.push(state)
		this.stateUser.set(username, userState)
	}

	public async clearUserState(username: string): Promise<void> {
		this.stateUser.set(username, { state: [], storage: {} } as state)
	}

	public async getStorage(username: string): Promise<storage> {
		const userState = this._getUserState(username)
		return userState.storage
	}

	public async setStorage(username: string, newStorage: storage): Promise<void> {
		const userState = this._getUserState(username)
		userState.storage = newStorage
		this.stateUser.set(username, userState)
	}

	private _getUserState(username: string): state {
		const state = this.stateUser.get(username)
		if (state) {
			return state
		} else {
			return { state: [] } as state
		}
	}
}
