import { Module } from '@nestjs/common'
import { ConfigModule } from '@nestjs/config'
import { DiscordModule, TransformPipe, ValidationPipe } from 'discord-nestjs'
import { discordConfig, envConfig } from './config'
import { YearManagementService } from './services/year-management/year-management.service'
import { TypeOrmModule } from '@nestjs/typeorm'
import { DbMasterConfig } from './config/db.master.config'
import { UserState } from './state/userState'
import { BotManagement } from './services/bot-management/bot-management.service'
import { UsersRepository } from './repositories/user.repository'
@Module({
	imports: [
		ConfigModule.forRoot(envConfig()),
		TypeOrmModule.forRootAsync({
			useClass: DbMasterConfig,
		}),
		TypeOrmModule.forFeature([UsersRepository]),
		DiscordModule.forRoot(discordConfig({ usePipes: [TransformPipe, ValidationPipe] })),
	],
	controllers: [],
	providers: [YearManagementService, UserState, BotManagement],
})
export class AppModule {}
