export function env<K extends keyof NodeJS.ProcessEnv>(key: K): NodeJS.ProcessEnv[K] {
	return process.env[key]
}
