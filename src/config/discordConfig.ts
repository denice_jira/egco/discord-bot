import { DiscordModuleOption } from 'discord-nestjs'
import { GuardType } from 'discord-nestjs/dist/core/util/type/guard-type'
import { PipeType } from 'discord-nestjs/dist/core/util/type/pipe-type'
import { env } from './env'

export interface DiscordConfigOptions {
	usePipes?: PipeType[]
	useGuards?: GuardType[]
}

export interface DiscordModuleOptionNew extends DiscordModuleOption {
	intents?: string[]
}

export function discordConfig(options: DiscordConfigOptions = {}): DiscordModuleOptionNew {
	return {
		commandPrefix: '',
		token: env('DISCORD_TOKEN'),
		intents: ['GUILDS', 'GUILD_MEMBERS', 'DIRECT_MESSAGES', 'GUILD_MESSAGES'],
		ws: { intents: ['GUILDS', 'GUILD_MEMBERS', 'DIRECT_MESSAGES', 'GUILD_MESSAGES'] },
		...options,
	}
}
