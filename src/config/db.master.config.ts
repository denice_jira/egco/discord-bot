import { TypeOrmModuleOptions, TypeOrmOptionsFactory } from '@nestjs/typeorm'
import { Injectable } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import * as entities from 'src/entity'

@Injectable()
export class DbMasterConfig implements TypeOrmOptionsFactory {
	constructor(private configService: ConfigService<NodeJS.EnvFile>) {}

	createTypeOrmOptions(): TypeOrmModuleOptions {
		// const logging = this.configService.get<string>('DB_LOGGING_ENABLE') ?? false

		return {
			name: 'default',
			type: 'postgres',
			// logging,
			host: this.configService.get<string>('DB_HOST'),
			port: parseInt(this.configService.get<string>('DB_PORT')),
			database: this.configService.get<string>('DB_NAME'),
			username: this.configService.get<string>('DB_USERNAME'),
			password: this.configService.get<string>('DB_PASS'),
			synchronize: true,
			entities: Object.values(entities),
			extra: {
				connectionLimit: 1,
			},
		}
	}
}
