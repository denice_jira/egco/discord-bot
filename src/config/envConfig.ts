import { ConfigModuleOptions } from '@nestjs/config'

export function envConfig(): ConfigModuleOptions {
	return {
		envFilePath: ['.env.local', '.env'],
		isGlobal: true,
	}
}
