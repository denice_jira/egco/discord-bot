import { Message } from 'discord.js'
import { createFilterDecorator } from './utils'

export const NoBot = (): MethodDecorator =>
	createFilterDecorator<Message, Message>({
		handleTransformer: parameter => !parameter.author.bot,
		requiredArgs: [Message],
	})
