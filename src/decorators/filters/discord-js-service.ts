import { OnDecoratorOptions } from 'discord-nestjs/dist/core/decorator/interface/on-decorator-options'
import { DecoratorTypes } from '../../enums/metadata-types.enum'
import { CheckChannel } from './check-channel'
import { registerFilterDecorators } from './utils'

export const DiscordJSService = (): ClassDecorator => {
	return (target): typeof target => {
		registerFilterDecorators<OnDecoratorOptions>(target, [
			{
				metadataKey: DecoratorTypes.On,
				matcher: { event: 'message' },
				decorator: CheckChannel(),
			},
		])

		return target
	}
}
