import { isMatch } from 'lodash'
import { Class } from 'utility-types'
import { MetadataTypes } from '../../enums/metadata-types.enum'
import { classInstanceOf } from '../../utils/classInstanceOf'
import { isFunction } from '../../utils/typeChecker'

// eslint-disable-next-line @typescript-eslint/no-explicit-any
type AnyFunction = (...args: any[]) => any

export interface CreateFilterDecoratorOptions<T, R, A = R> {
	parameterFilter?: (parameterType: Class<unknown>) => parameterType is Class<T>
	parameterMap?: (parameter: Class<T>) => Class<R>
	handleTransformer: (parameter: R, original: Class<T>) => [true, A] | [false, A] | [false] | boolean
	requiredArgs?: Class<T>[]
}

export function createFilterDecorator<T, R, A = R>(options: CreateFilterDecoratorOptions<T, R, A>): MethodDecorator {
	return <V>(target, propertyKey, descriptor: TypedPropertyDescriptor<V>): TypedPropertyDescriptor<V> => {
		const argsType: Class<T | unknown>[] | undefined = Reflect.getMetadata(MetadataTypes.Paramtypes, target, propertyKey)
		const { value } = descriptor

		if (!argsType) return descriptor

		const argIndex = argsType.map((argType, index) => (options.parameterFilter?.(argType) !== false ? index : -1)).filter(index => index !== -1)
		if (argIndex.length === 0) return descriptor

		const mappedArgsType = argsType.map((argType, index) =>
			options.parameterMap && argIndex.includes(index) ? options.parameterMap(argType as Class<T>) : argType,
		)

		const additionalArgs =
			options.requiredArgs?.filter(requiredArg => mappedArgsType.every(mappedArgType => !classInstanceOf(mappedArgType, requiredArg))) ?? []

		Reflect.defineMetadata(MetadataTypes.Paramtypes, [...mappedArgsType, ...additionalArgs], target, propertyKey)

		return {
			...descriptor,
			value: isFunction(value)
				? (async function (...args) {
						const [shouldHandle, mappedArgs] = args.slice(0, args.length - additionalArgs.length).reduce(
							([accShouldHandle, accArgs], arg, index) => {
								if (argIndex.includes(index)) {
									const handleTransformerReturn = options.handleTransformer(arg, argsType[index] as Class<T>)
									if (Array.isArray(handleTransformerReturn))
										return [accShouldHandle || handleTransformerReturn[0], [...accArgs, handleTransformerReturn[1]]]
									return [accShouldHandle || handleTransformerReturn, [...accArgs, arg]]
								}

								return [accShouldHandle, [...accArgs, arg]]
							},
							[false, []],
						)

						if (shouldHandle) return value.call(this, ...mappedArgs)
						return undefined
				  } as V extends AnyFunction ? V : never)
				: descriptor.value,
		}
	}
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export interface RegisterFilterDecoratorOptions<T> {
	metadataKey: string
	matcher: T extends AnyFunction ? never : T | (() => T)
	decorator: MethodDecorator | MethodDecorator[]
}

function matchMetaData(metaData: unknown, matcher: unknown): boolean {
	if (typeof metaData === 'object') {
		if (typeof matcher === 'object') {
			return isMatch(metaData, matcher)
		} else {
			return false
		}
	} else {
		return metaData === matcher
	}
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any, @typescript-eslint/ban-types
export function registerFilterDecorator<T>(target: Function, options: RegisterFilterDecoratorOptions<T>): void {
	Object.getOwnPropertyNames(target.prototype).forEach(key => {
		if (!isFunction(target.prototype[key])) return
		const metaData = Reflect.getMetadata(options.metadataKey, target.prototype, key)
		const { matcher } = options
		if ((matcher !== undefined && metaData === undefined) || (matcher === undefined && metaData !== undefined)) return

		if (!matchMetaData(metaData, typeof matcher === 'function' ? matcher() : matcher)) return

		Object.defineProperty(
			target.prototype,
			key,
			Reflect.decorate(
				Array.isArray(options.decorator) ? options.decorator : [options.decorator],
				target.prototype,
				key,
				Reflect.getOwnPropertyDescriptor(target.prototype, key),
			),
		)
	})
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any, @typescript-eslint/ban-types
export function registerFilterDecorators<T>(target: Function, options: RegisterFilterDecoratorOptions<T>[]): void {
	options.forEach(option => registerFilterDecorator(target, option))
}
