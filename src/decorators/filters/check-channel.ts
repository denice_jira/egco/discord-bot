import { Message } from 'discord.js'
import { Class } from 'utility-types'
import { DMMessage, NewsMessage, TextMessage } from '../../types/Message.type'
import { classInstanceOf } from '../../utils/classInstanceOf'
import { createFilterDecorator } from './utils'

function checkMessageType<T extends Message>(message: Message, type: Class<T>): message is T {
	switch (type) {
		case TextMessage as Class<Message>:
			return message.channel.type === 'text'
		case NewsMessage as Class<Message>:
			return message.channel.type === 'news'
		case DMMessage as Class<Message>:
			return message.channel.type === 'dm'
		default:
			return false
	}
}

export const CheckChannel = (): MethodDecorator =>
	createFilterDecorator<Message, Message>({
		parameterFilter: (parameterType): parameterType is Class<Message> => classInstanceOf(parameterType, Message),
		parameterMap: () => Message,
		handleTransformer: (parameter, original) => checkMessageType(parameter, original),
		requiredArgs: [Message],
	})
