import { Test, TestingModule } from '@nestjs/testing'
import { YearManagementService } from '../../../src/services/year-management/year-management.service'

describe('YearManagementService', () => {
	let service: YearManagementService

	beforeEach(async () => {
		const module: TestingModule = await Test.createTestingModule({
			providers: [YearManagementService],
		}).compile()

		service = module.get<YearManagementService>(YearManagementService)
	})

	it('should be defined', () => {
		expect(service).toBeDefined()
	})
})
